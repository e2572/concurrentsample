﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrentSample.Semaphore
{
    /// <summary>
    /// https://dotnettutorials.net/lesson/semaphoreslim-class-in-csharp/
    /// SemaphoreSlim use to Limiting concurrency
    /// </summary>
    public class SemaphoreSlimSample
    {
        // A padding interval to make the output more orderly.
        private int padding;

        public void RunSomethingWithSemaphoreSlim()
        {
            // Create the semaphore.
            SemaphoreSlim semaphore = new SemaphoreSlim(0, 3);
       
            Console.WriteLine("{0} tasks can enter the semaphore.",
                                  semaphore.CurrentCount);
            Task[] tasks = new Task[5];

            // Create and start five numbered tasks.
            for (int i = 0; i <= 4; i++)
            {
                tasks[i] = Task.Run(() =>
                {
                    // Each task begins by requesting the semaphore.
                    Console.WriteLine($"{DateTime.Now} Task {Task.CurrentId} begins and waits for the semaphore.");

                    int semaphoreCount;
                    semaphore.Wait();
                    try
                    {
                        Interlocked.Add(ref padding, 100);

                        Console.WriteLine("{1} Task {0} enters the semaphore.padding is {2}", Task.CurrentId,DateTime.Now, padding);

                        // The task just sleeps for 1+ seconds.
                        Thread.Sleep(1000 + padding);
                    }
                    finally
                    {
                        semaphoreCount = semaphore.Release();
                    }
                    Console.WriteLine("{2} Task {0} releases the semaphore; previous count: {1}.",
                                      Task.CurrentId, semaphoreCount,DateTime.Now);
                });
            }

            // Wait for half a second, to allow all the tasks to start and block.
            Thread.Sleep(500);

            // Restore the semaphore count to its maximum value.
            Console.Write($"{DateTime.Now} Main thread calls Release(3) --> ");
            semaphore.Release(3);
            Console.WriteLine("{1} {0} tasks can enter the semaphore.",
                              semaphore.CurrentCount, DateTime.Now);
            // Main thread waits for the tasks to complete.
            Task.WaitAll(tasks);

            Console.WriteLine("Main thread exits.");
        }

        /// <summary>
        /// run specail tasknum with SemaphoreSlim
        /// </summary>
        /// <param name="taskNums"></param>
        public void RunSpecailTaskWithSemaphoreSlim(int taskNums)
        {
            // Create the semaphore.
            var semaphore = new SemaphoreSlim(0, taskNums);
            semaphore.Release(taskNums);

            while (true)
            {
                semaphore.Wait();
                Task.Run(() =>
                {
                    int semaphoreCount;
                    try
                    {
                        Console.WriteLine("{1} Task {0} enters the semaphore.", Task.CurrentId, DateTime.Now);

                        // The task just sleeps for 5 second.
                        Thread.Sleep(5000);
                    }
                    finally
                    {
                        semaphoreCount = semaphore.Release();
                    }

                    Console.WriteLine("{2} Task {0} releases the semaphore; previous count: {1}.",
                                     Task.CurrentId, semaphoreCount, DateTime.Now);
                });
           
            }
        }
    }
}
