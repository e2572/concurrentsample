﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrentSample.Semaphore
{
    /// <summary>
    /// Semaphore 使外部线程变得同步
    /// https://dotnettutorials.net/lesson/semaphore-in-multithreading/
    /// </summary>
    public class SemaphoreSample
    {
        public System.Threading.Semaphore semaphore = null;

        public SemaphoreSample()
        {

        }

        /// <summary>
        /// 该案例限制只允许开启两个进程
        /// </summary>
        public void RunSemaphoreApp()
        {

            /*
             跨进程线程控制，提供名称
             */
            System.Threading.Semaphore semaphore = null;

            try
            {
                //Try to Open the Semaphore if Exists, if not throw an exception
                semaphore = System.Threading.Semaphore.OpenExisting("ConcurrentSampleDemo");
            }
            catch (Exception ex)
            {
                //If Semaphore not Exists, create a semaphore instance
                //Here Maximum 2 external threads can access the code at the same time
                semaphore = new System.Threading.Semaphore(2, 2, "ConcurrentSampleDemo");
            }
            Console.WriteLine("External Thread Trying to Acquiring");
            semaphore.WaitOne();
            //This section can be access by maximum two external threads: Start
            Console.WriteLine("External Thread Acquired");
            Console.ReadKey();
            //This section can be access by maximum two external threads: End
            semaphore.Release();
        }

        public void RunSemaphoreToDoSomeTask()
        {
            /*
            用法和SemaphoreSlim 相似
             */

            System.Threading.Semaphore semaphore = new System.Threading.Semaphore(2, 3);

            for (int i = 1; i <= 10; i++)
            {
                var threadObject = new Thread(DoSomeTask)
                {
                    Name = "Thread " + i
                };
                threadObject.Start();
            }
            Console.ReadKey();

            void DoSomeTask()
            {
                Console.WriteLine(Thread.CurrentThread.Name + " Wants to Enter into Critical Section for processing");
                try
                {
                    //Blocks the current thread until the current WaitHandle receives a signal.   
                    semaphore.WaitOne();
                    //Decrease the Initial Count Variable by 1
                    Console.WriteLine("Success: " + Thread.CurrentThread.Name + " is Doing its work");
                    Thread.Sleep(5000);
                    Console.WriteLine(Thread.CurrentThread.Name + "Exit.");
                }
                finally
                {
                    //Release() method to release semaphore  
                    //Increase the Initial Count Variable by 1
                    semaphore.Release();
                }
            }
        }     
    }
}
