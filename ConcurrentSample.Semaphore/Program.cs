﻿using System;

namespace ConcurrentSample.Semaphore
{
    class Program
    {
        static void Main(string[] args)
        {
            RunSemaphoreSample();
            //RunSemaphoreSlimSample()
            Console.ReadKey();
        }

        static void RunSemaphoreSlimSample()
        {
            //new SemaphoreSlimSample().RunSpecailTaskWithSemaphoreSlim(2);
            new SemaphoreSlimSample().RunSomethingWithSemaphoreSlim();
        }

        static void RunSemaphoreSample()
        {
            new SemaphoreSample().RunSemaphoreApp();
        }
    }
}
