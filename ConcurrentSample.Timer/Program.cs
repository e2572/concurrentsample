﻿using System;

namespace ConcurrentSample.Timer
{
    class Program
    {
        static void Main(string[] args)
        {
            new SerialThreadTimer().Start();
            //new ParallelThreadTimer().Start();
            Console.ReadKey();
        }
    }
}
