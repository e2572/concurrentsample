﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrentSample.Timer
{
    /// <summary>
    /// SerialThreadTimer 串行模式下的Timer,如果中间某个任务没执行完,会一直等待。
    /// </summary>
    public class SerialThreadTimer
    {
        private System.Threading.Timer _timer;
        private const int Interval = 1000;
        private int Count = 0;
        public SerialThreadTimer()
        {
        }

        public void Start()
        {
            // 注意这里的 period 是 Timeout.Infinite 表示无限等待
            _timer = new System.Threading.Timer(ToDo, null, Interval, Timeout.Infinite);
        }

        private void ToDo(object obj)
        {
            // sleep 3 seconds
            Thread.Sleep(3000);

            Interlocked.Increment(ref Count);
            // run after 1 seconds
            _timer.Change(Interval, Timeout.Infinite);

            // 这里的线程池Id会不一样。Timer 的每一次执行,都会从TaskSheduler 分配一个Task 去执行
            Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss.fff")} Current count is {Count},CurrentThreadId is {Thread.CurrentThread.ManagedThreadId}");
            if (Count == 10)
            {
                _timer.Dispose();
                Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss.fff")} Timer disposed.");
            }
        }
    }
}
