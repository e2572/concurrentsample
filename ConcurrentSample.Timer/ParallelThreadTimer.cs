﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConcurrentSample.Timer
{
    /// <summary>
    /// 按时执行，没有等待，会有多个任务同时在运行
    /// </summary>
    public class ParallelThreadTimer
    {
        private System.Threading.Timer _timer;
        private const int Interval = 1000;
        private int Count = 0;

        public ParallelThreadTimer()
        { 
            
        }

        public void Start()
        {
            // 每1秒执行一次,没有等待
            _timer = new System.Threading.Timer(ToDo, null, Interval, Interval);
        }

        public void ToDo(object obj)
        {
            Interlocked.Increment(ref Count);
            // different managedThreadId   Timer 的每一次执行,都会从TaskSheduler 分配一个Task 去执行
            Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss.fff")} Current count is {Count},CurrentThreadId is {Thread.CurrentThread.ManagedThreadId}");
            if (Count == 10)
            {
                _timer.Dispose();
                Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss.fff")} Timer disposed.");
            }

            // sleep in the last
            Thread.Sleep(2000);
        }
    }
}
