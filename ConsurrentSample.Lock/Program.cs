﻿using System;

namespace ConsurrentSample.Lock
{
    class Program
    {
        static void Main(string[] args)
        {
            //RunMutexSample();
            //RunMonitorSample();
            RunLockSample();
            Console.ReadKey();
        }

        static void RunMutexSample()
        {
            new MutexSample().RunMutilThreadWithMutex();
        }

        static void RunMonitorSample()
        {
            //new MonitorSample().StartPrintNumbersInTreads();
            new MonitorSample().StartPrintNumbersWithSWaitAndpulse();
        }

        static void RunLockSample()
        {
            new LockSample().StartPrintCounts();
        }
    }
}
