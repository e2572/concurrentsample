﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsurrentSample.Lock
{
    public class LockSample
    {
        private readonly object lockObj = new object();

        public LockSample()
        { 
            
        }

        public void StartPrintCounts()
        {
            int count = 0;
            var t1 = new Thread(Count);
            var t2 = new Thread(Count);
            var t3 = new Thread(Count);

            t1.Start();
            t2.Start();
            t3.Start();

            //Wait for all three threads to complete their execution
            t1.Join();
            t2.Join();
            t3.Join();

            Console.WriteLine($"Completed Count is {count}");
            Console.ReadLine();

            void Count()
            {
                for (int i = 0; i < 10000; i++)
                {
                    // with lock the count is 30000
                    lock (lockObj)
                    {
                        try
                        {
                            // 执行可能引发异常的操作
                            count++;
                        }
                        catch (Exception ex)
                        {
                            // 处理异常
                        }
                        // 在这里释放锁 
                    }

                    // without lock the count is less than 10000
                    //count++;
                }
            }
        }
    }
}
