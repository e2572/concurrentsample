﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsurrentSample.Lock
{
    /// <summary>
    /// 多进程之间的单线程锁
    /// https://dotnettutorials.net/lesson/mutex-in-multithreading/
    /// </summary>
    public class MutexSample
    {
        /*
         Mutex 在以下情况下非常有用：

        全局资源管理： 当多个进程需要协调对某个全局资源（如共享文件、共享内存、设备等）的访问时，可以使用 Mutex 来确保只有一个进程可以访问该资源。

        跨进程同步： Mutex 具有全局性，因此可以用于跨不同进程的线程同步。这对于需要在不同应用程序之间共享资源的情况非常有用。

        定时任务协调： 如果你有多个定时任务需要协调执行，可以使用 Mutex 来确保它们不会同时执行。

        线程安全性： 在某些情况下，你可能需要确保只有一个线程可以执行特定代码块，以确保线程安全性。Mutex 可以用于这种情况。

        需要注意的是，Mutex 是一种较重量级的同步机制，通常比其他同步原语如 Monitor 或 lock 更耗费系统资源。
        因此，在使用 Mutex 时，需要谨慎考虑性能和资源开销。如果只需要在单个应用程序内的多个线程之间同步，通常情况下，Monitor 或 lock 更为适合。
        但如果需要跨进程同步，那么Mutex 是一个合适的选择。
         */


        private Mutex _mutex;

        public MutexSample()
        {
            _mutex = new Mutex(false, "MutexSample");
        }

        public void RunMutilThreadWithMutex()
        {
            var process = System.Diagnostics.Process.GetCurrentProcess();
            for (int i = 1; i <= 5; i++)
            {
                Thread threadObject = new Thread(MutexDemo)
                {
                    Name = $"Process {process.Id}, Thread {i}"
                };
                threadObject.Start();
            }
        }

        void MutexDemo()
        {
            Console.WriteLine(Thread.CurrentThread.Name + " Wants to Enter Critical Section for processing");
            try
            {
                //Blocks the current thread until the current WaitOne method receives a signal.  
                //Wait until it is safe to enter. 
                _mutex.WaitOne();
                Console.WriteLine($" {DateTime.Now.ToString("HH:mm:ss fff")} Success: {Thread.CurrentThread.Name} is Processing now");
                Thread.Sleep(5000);
                Console.WriteLine($"{DateTime.Now.ToString("HH:mm:ss fff")} Exit: {Thread.CurrentThread.Name}is Completed its task");
            }
            finally
            {
                //Call the ReleaseMutex method to unblock so that other threads
                //that are trying to gain ownership of the mutex can enter  
                _mutex.ReleaseMutex();
            }
        }
    }
}
