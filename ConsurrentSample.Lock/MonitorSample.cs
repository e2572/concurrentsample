﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsurrentSample.Lock
{
    /// <summary>
    /// https://dotnettutorials.net/lesson/multithreading-using-monitor/
    /// </summary>
    public class MonitorSample
    {
        /* 使用场景：

        Monitor 类通常用于以下情况：

        1、多线程访问共享资源： 当多个线程需要同时访问一个共享资源（如共享变量、数据结构等）时，你可以使用 Monitor 来确保一次只有一个线程可以访问该资源，以避免数据竞争和一致性问题。

        2、线程安全的数据结构： 如果你正在实现自己的线程安全数据结构，例如线程安全的队列或堆栈，Monitor 可以用于同步对数据结构的访问。

        3、避免竞态条件： 在需要执行一系列原子操作的情况下，Monitor 可以用于创建一个临界区，确保这些操作在同一个线程中执行，从而避免竞态条件。

        4、等待-通知模式： Monitor 也可以用于实现等待-通知模式，其中一个线程等待某个条件满足，而其他线程在满足条件时通知等待线程。

        需要注意的是，Monitor 是一种较低级别的同步机制，需要手动管理锁，因此在使用时需要谨慎，以避免死锁等问题。在某些情况下，使用高级别的同步构造如 lock 关键字可能更加方便和安全。
        */


        private readonly object lockObj = new object();

        public MonitorSample()
        { 
            
        }

        public void StartPrintNumbersInTreads()
        {
            Thread[] Threads = new Thread[3];
            for (int i = 0; i < 3; i++)
            {
                var td = new Thread(PrintNumbersWithTimeOut)
                {
                    Name = "Child Thread " + i
                };
                if (i == 2)
                {
                    td.Priority = ThreadPriority.Highest;
                }
                Threads[i] = td;
            }
            foreach (Thread t in Threads)
            {
                t.Start();
            }
            Console.ReadLine();
        }

        /// <summary>
        ///  可以设置一个超时时间，以避免无限等待锁。
        /// </summary>
        public void PrintNumbersWithTimeOut()
        {
            TimeSpan timeout = TimeSpan.FromMilliseconds(1000);
            bool lockTaken = false;

            try
            {
                Console.WriteLine(Thread.CurrentThread.Name + " Trying to enter into the critical section");
                Thread.Sleep(200);
                // The amount of time to wait for the lock
                Monitor.TryEnter(lockObj, timeout, ref lockTaken);
                if (lockTaken)
                {
                    Console.WriteLine(Thread.CurrentThread.Name + " Entered into the critical section");
                    for (int i = 0; i < 5; i++)
                    {
                        Thread.Sleep(300);
                        Console.Write(i + ",");
                    }
                    Console.WriteLine(Environment.NewLine);
                }
                else
                {
                    // The lock was not acquired.
                    Console.WriteLine(Thread.CurrentThread.Name + " Lock was not acquired");
                }
            }
            finally
            {
                // To Ensure that the lock is released.
                if (lockTaken)
                {
                    Monitor.Exit(lockObj);
                    Console.WriteLine(Thread.CurrentThread.Name + " Exit from critical section");
                    Console.WriteLine(Environment.NewLine);
                }
            }
        }

        private void PrintNumbers()
        {
            Console.WriteLine(Thread.CurrentThread.Name + "Trying to enter into the critical section");
            try
            {
                Monitor.Enter(lockObj);
                Thread.Sleep(200);
                Console.WriteLine(Thread.CurrentThread.Name + " Entered into the critical section");
                for (int i = 0; i < 5; i++)
                {
                    Thread.Sleep(200);
                    Console.Write(i + ",");
                }
                Console.WriteLine();               
            }
            finally
            {
                Monitor.Exit(lockObj);
                Console.WriteLine(Thread.CurrentThread.Name + " Exit from critical section");
                Console.WriteLine(Environment.NewLine);
            }
        }

        /// <summary>
        /// print odd and even numbers in different threads
        /// </summary>
        public void StartPrintNumbersWithSWaitAndpulse()
        {
            // Upto the limit numbers will be printed on the Console
            int numberLimit = 20;

            var EvenThread = new Thread(PrintEvenNumbers);
            var OddThread = new Thread(PrintOddNumbers);

            //First Start the Even thread.
            EvenThread.Start();

            //Puase for 10 ms, to make sure Even thread has started 
            //or else Odd thread may start first resulting different sequence.
            Thread.Sleep(100);

            //Next, Start the Odd thread.
            OddThread.Start();

            //Wait for all the childs threads to complete
            OddThread.Join();
            EvenThread.Join();

            Console.WriteLine("\nMain method completed");
            Console.ReadKey();

            void PrintEvenNumbers()
            {
                try
                {
                    //Implement lock as the Console is shared between two threads
                    Monitor.Enter(lockObj);
                    Console.WriteLine("Begin to print even numbers...");
                    for (int i = 0; i <= numberLimit; i = i + 2)
                    {
                        //Printing Even Number on Console)
                        Console.WriteLine($"event thread >>{i} ");

                        //Notify Odd thread that I'm done, you do your job
                        //It notifies a thread in the waiting queue of a change in the 
                        //locked object's state.
                        Monitor.Pulse(lockObj);

                        //I will wait here till Odd thread notify me 
                        //Monitor.Wait(monitor);
                        //Without this logic application will wait forever

                        bool isLast = false;
                        if (i == numberLimit)
                        {
                            isLast = true;
                        }

                        if (!isLast)
                        {
                            //I will wait here till Odd thread notify me
                            //Releases the lock on an object and blocks the current thread 
                            //until it reacquires the lock.
                            Monitor.Wait(lockObj);
                        }
                    }
                }
                finally
                {
                    //Release the lock
                    Monitor.Exit(lockObj);
                }

            }

            //Printing of Odd Numbers Function
            void PrintOddNumbers()
            {
                try
                {
                    //Hold lock as the Console is shared between two threads
                    Monitor.Enter(lockObj);
                    Console.WriteLine("Begin to print odd numbers...");
                    for (int i = 1; i <= numberLimit; i = i + 2)
                    {
                        //Printing the odd numbers on the console
                        Console.WriteLine($"odd threads >> {i} ");

                        //Notify Even thread that I'm done, you do your job
                        Monitor.Pulse(lockObj);

                        // I will wait here till even thread notify me
                        // Monitor.Wait(monitor);
                        // without this logic application will wait forever

                        bool isLast = false;
                        if (i == numberLimit - 1)
                        {
                            isLast = true;
                        }

                        if (!isLast)
                        {
                            //I will wait here till Even thread notify me
                            Monitor.Wait(lockObj);
                        }
                    }
                }
                finally
                {
                    //Release lock
                    Monitor.Exit(lockObj);
                }
            }
        }
    }
}
